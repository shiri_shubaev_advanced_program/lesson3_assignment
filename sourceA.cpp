#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

int main(void)
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		cout << "Can't open DB: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("pause");
		return(1);
	}

	rc = sqlite3_exec(db, "create table people(id integer primary key autoincremen, name TEXT);", NULL, 0, &zErrMsg); //making a table

	rc = sqlite3_exec(db, "insert into people(name) values('Jasmine');", NULL, 0, &zErrMsg); //entering 3 people
	rc = sqlite3_exec(db, "insert into people(name) values('Hadar');", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "insert into people(name) values('Sharon');", NULL, 0, &zErrMsg);

}
