#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>

using namespace std;

int callback_balance(void* notUsed, int argc, char** argv, char** azCol);
int callback_price(void* notUsed, int argc, char** argv, char** azCol);
int callback_available(void* notUsed, int argc, char** argv, char** azCol);
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);

int avlbl = 0;
int balance = 0;
int price = 0;

void main()
{
	int buyerId = 0;
	int carId = 0;
	char *zErrMsg = 0;

	cout << "Enter buyer ID:";
	cin >> buyerId;
	cout << "Enter car ID:";
	cin >> carId;

	sqlite3* db = NULL;
	
	int ans;
	ans = carPurchase(buyerId, carId, db, zErrMsg);

	if (ans == 1)
	{
		cout << "Transaction is possible" << endl;
	}
	else
	{
		if (price > balance)
		{
			cout << "Transaction cancelled: Buyer's credit balance is too low" << endl;
		}
		else
		{
			cout << "Transaction cancelled: Car is not available" << endl;
		}
	}
}

/*
function gets database, id of car, id of buyer
checks if the buyer can get the car
if he can, the function makes the purchace and updates all the information.
*/
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc;
	int rc2;

	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Cant open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	string str = "SELECT available FROM cars WHERE id=";
	str += to_string(carid);
	str += ';';
	cout << str << endl;

	const char* request = str.c_str();
	cout << request << endl;

	rc = sqlite3_exec(db, request, callback_available, NULL, &zErrMsg);

	if (avlbl == 0)
	{
		price = 0;
		balance = 0;
		return 0;
	}

	str = "SELECT price FROM cars WHERE id=";
	str += to_string(carid);
	str += ';';
	request = str.c_str();

	rc = sqlite3_exec(db, request, callback_price, NULL, &zErrMsg);

	str = "select balance from accounts where buyer_id=";
	str += to_string(buyerid);
	str += ';';
	request = str.c_str();

	rc = sqlite3_exec(db, request, callback_balance, NULL, &zErrMsg);

	if (price > balance)
	{
		return 0;
	}
	else
	{
		str = "begin";
		str += ';';
		request = str.c_str();

		rc = sqlite3_exec(db, request, callback_price, 0, &zErrMsg);
		
		str = "update cars set available=0 where id=";
		str += to_string(carid);
		str += ';';
		request = str.c_str();
		
		rc = sqlite3_exec(db, request, callback_price, 0, &zErrMsg);
		
		balance -= price;
		str = "update accounts set balance=";
		str += to_string(balance);
		str += " where id=";
		str += to_string(buyerid);
		str += ';';
		request = str.c_str();
		
		rc = sqlite3_exec(db, request, callback_price, 0, &zErrMsg);
		
		str = "commit";
		str += ';';
		request = str.c_str();
		
		rc = sqlite3_exec(db, request, callback_price, 0, &zErrMsg);
		
		return 1;
	}
}

/*
callback function for price in cars
*/
int callback_price(void* notUsed, int argc, char** argv, char** azCol)
{
	string str(argv[0]);
	price = std::stoi(str);
	return 0;
}

//callback function for available in cars
int callback_available(void* notUsed, int argc, char** argv, char** azCol)
{
	avlbl = int(*argv[0]) - 48;
	return 0;
}

//callback function for balance in accounts
int callback_balance(void* notUsed, int argc, char** argv, char** azCol)
{
	string str(argv[0]);
	balance = std::stoi(str);
	return 0;
}

